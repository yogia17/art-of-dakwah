<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
class HomeController extends Controller
{
    public function home($value='')
    {
    	return view('welcome');
    }
    public function add($value='')
    {
    	return view('add');
    }

    public function store(Request $request){
        $validator = Validator::make($request->all(), ['hp' => 'required|unique:users','first_name' => 'required']);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator->errors());
        }

       $user = DB::table('users')->insert(
            ['first_name' => $request->first_name, 'hp' => $request->hp, 'h1s1' => date('Y-m-d H:i:s')]
        );

        return redirect()->route('homepage');
    }
    public function YourhomePage($value='')
    {
    	return view('home');
    }
    public function dashboard($value='')
    {
    	return view('backEnd.dashboard');
    }
}
