<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AODController extends Controller
{
    public function index()
    {
    	return view('auth.aodLogin');
    }
    public function laporan()
    {
        $user = User::orderby('h1s1','desc')->get();
    	return view('laporan',compact('user'));
    }
    public function step2(Request $request)
    {
        $messages = [
            'exists' => ':attribute belum terdaftar.',
        ];
        $validator = Validator::make($request->all(), ['hp' => 'exists:users|required'],$messages);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator->errors());
        }else{

            $hp = $request->input('hp');
            if(empty($hp)){
                echo 'No. HP harus diisi';
                die();
            }
            $user = User::where('hp',$hp)->first();
            if (!$user) {
                echo 'Ho. HP belum terdaftar. Silakan hubungi panitia!';
                die();
            }

            return view('auth.aodQR',compact('hp'));
        }
    }
    public function presensi($hp)
    {
        $user = User::where('hp',$hp)->first();
        if(!$user){
            echo 'No HP tidak terdaftar.';
            die();
        }
        return view('presensi',compact('user'));
    }

}
