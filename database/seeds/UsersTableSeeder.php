<?php

use Illuminate\Database\Seeder;

class UsersGenerate extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [		'id' 			=> '1',
                'hp' 		=> '082323722430',
                'first_name' 			=> 'Nurdianto'

            ],
            [		'id' 			=> '2',
                'hp' 		=> '087738837009',
                'first_name' 			=> 'Muhammad Ilyas'
            ],
            [		'id' 			=> '3',
                'hp' 		=> '082331639100',
                'first_name' 			=> 'Supomo'
            ],
            [		'id' 			=> '4',
                'hp' 		=> '082242016612',
                'first_name' 			=> 'Bambang Susanto'
            ],
            [		'id' 			=> '5',
                'hp' 		=> '08112570252',
                'first_name' 			=> 'Doni Riwayanto'
            ],
            [		'id' 			=> '6',
                'hp' 		=> '087739488717',
                'first_name' 			=> 'Anaris dwiyanta'
            ],
            [		'id' 			=> '7',
                'hp' 		=> '081353447656',
                'first_name' 			=> 'Nur Latif'
            ],
            [		'id' 			=> '8',
                'hp' 		=> '081229026314',
                'first_name' 			=> 'Lintang wisnuhadi Bawono '
            ],
            [		'id' 			=> '9',
                'hp' 		=> '082375797165',
                'first_name' 			=> 'Nurdin Adiyansah'
            ],
            [		'id' 			=> '10',
                'hp' 		=> '081369580884',
                'first_name' 			=> 'Riski Wahyu Agung '
            ],
            [		'id' 			=> '11',
                'hp' 		=> '085727770422',
                'first_name' 			=> 'Muhammad Fachruddin b'
            ],
            [		'id' 			=> '12',
                'hp' 		=> '081392879392',
                'first_name' 			=> 'Ferdy Hetarihon'
            ],
            [		'id' 			=> '13',
                'hp' 		=> '081226917577',
                'first_name' 			=> 'Fadhil Rachmadi '
            ],
            [		'id' 			=> '14',
                'hp' 		=> '082293508330',
                'first_name' 			=> 'arul Suranto'
            ],
            [		'id' 			=> '15',
                'hp' 		=> '0852418089719',
                'first_name' 			=> 'Yarfin'
            ],
            [		'id' 			=> '16',
                'hp' 		=> '087734775700',
                'first_name' 			=> 'Muhamad Puspa Alam Tahta Perlindungan'
            ],
            [		'id' 			=> '17',
                'hp' 		=> '081328805673',
                'first_name' 			=> 'Kusumo Aji Saputo'
            ],
            [		'id' 			=> '18',
                'hp' 		=> '085822425767',
                'first_name' 			=> 'Arief sunanda'
            ],
            [		'id' 			=> '19',
                'hp' 		=> '085700215025',
                'first_name' 			=> 'Supriyadi'
            ],
            [		'id' 			=> '20',
                'hp' 		=> '081266300557',
                'first_name' 			=> 'Deny Syahputra '
            ],
            [		'id' 			=> '21',
                'hp' 		=> '081356388822',
                'first_name' 			=> 'Rizki Ilman Hidayanto'
            ],
            [		'id' 			=> '22',
                'hp' 		=> '082396530117',
                'first_name' 			=> 'La Pili La Bae'
            ],
            [		'id' 			=> '23',
                'hp' 		=> '085655867572',
                'first_name' 			=> 'Delis Bagus'
            ],
            [		'id' 			=> '24',
                'hp' 		=> '085855025672',
                'first_name' 			=> 'Ardi Setiawan'
            ],
            [		'id' 			=> '25',
                'hp' 		=> '089652152699',
                'first_name' 			=> 'KHOLIDUN '
            ],
            [		'id' 			=> '26',
                'hp' 		=> '081327565388',
                'first_name' 			=> 'Irsyad Panca Gunawan'
            ],
            [		'id' 			=> '27',
                'hp' 		=> '082138562767',
                'first_name' 			=> 'M. Ichsan Rasyidi'
            ],
            [		'id' 			=> '28',
                'hp' 		=> '082133467318',
                'first_name' 			=> 'Redha bagastamatiin sindarotama '
            ],
            [		'id' 			=> '29',
                'hp' 		=> '081578757081',
                'first_name' 			=> 'Wahyu Dianto'
            ],
            [		'id' 			=> '30',
                'hp' 		=> '083843611986',
                'first_name' 			=> 'Istian Muhammad Wahyu Setiawan'
            ],
            [		'id' 			=> '31',
                'hp' 		=> '085701243154',
                'first_name' 			=> 'Okta fandrian'
            ],
            [		'id' 			=> '32',
                'hp' 		=> '081219942629',
                'first_name' 			=> 'Wiki andri'
            ],
            [		'id' 			=> '33',
                'hp' 		=> '082312225580',
                'first_name' 			=> 'Yogi Anggriawan'
            ],
            [		'id' 			=> '34',
                'hp' 		=> '082337851174',
                'first_name' 			=> 'DHANANG HANIS ADI KUSUMA'
            ],
            [		'id' 			=> '35',
                'hp' 		=> '085356403324',
                'first_name' 			=> 'Zulhamdi Shaleh PA'
            ],
            [		'id' 			=> '36',
                'hp' 		=> '081283047537',
                'first_name' 			=> 'Agung Mur Pratomo'
            ],
            [		'id' 			=> '37',
                'hp' 		=> '082299315470',
                'first_name' 			=> 'Dodi Saputra'
            ],
            [		'id' 			=> '38',
                'hp' 		=> '08157927974',
                'first_name' 			=> 'Aris wahyudi'
            ],
            [		'id' 			=> '39',
                'hp' 		=> '081252757353',
                'first_name' 			=> 'Sigit Aribowo'
            ],
            [		'id' 			=> '40',
                'hp' 		=> '085261273323',
                'first_name' 			=> 'Irsyadi Alfarabi'
            ],
            [		'id' 			=> '41',
                'hp' 		=> '08981010397',
                'first_name' 			=> 'zul naro alam nauli'
            ],
            [		'id' 			=> '42',
                'hp' 		=> '082393393519',
                'first_name' 			=> 'Maulana M'
            ],
            [		'id' 			=> '43',
                'hp' 		=> '085724868230',
                'first_name' 			=> 'Muhammad Ali Dakir'
            ],
            [		'id' 			=> '44',
                'hp' 		=> '085292575033',
                'first_name' 			=> 'Nana Widiyanto'
            ],
            [		'id' 			=> '45',
                'hp' 		=> '081547274525',
                'first_name' 			=> 'husen efendi'
            ],
            [		'id' 			=> '46',
                'hp' 		=> '088806158203',
                'first_name' 			=> 'Wahono'
            ],
            [		'id' 			=> '47',
                'hp' 		=> '08121562256',
                'first_name' 			=> 'Wahid Ar.'
            ],
            [		'id' 			=> '48',
                'hp' 		=> '081392826668',
                'first_name' 			=> 'alfi saptana'
            ],
            [		'id' 			=> '49',
                'hp' 		=> '081227444555',
                'first_name' 			=> 'Syahrul G Wijaya'
            ],
            [		'id' 			=> '50',
                'hp' 		=> '085725972247',
                'first_name' 			=> 'Deviyanto '
            ],
            [		'id' 			=> '51',
                'hp' 		=> '082126131963',
                'first_name' 			=> 'Anwar Udin'
            ],
            [		'id' 			=> '52',
                'hp' 		=> '082153621036',
                'first_name' 			=> 'Megi Saputra '
            ],
            [		'id' 			=> '53',
                'hp' 		=> '081328025019',
                'first_name' 			=> 'Agus Purnomo'
            ],
            [		'id' 			=> '54',
                'hp' 		=> '081328779088',
                'first_name' 			=> 'Isna bahyang mahadi'
            ],
            [		'id' 			=> '55',
                'hp' 		=> '081227898505',
                'first_name' 			=> 'Agusti Abdillah'
            ],
            [		'id' 			=> '56',
                'hp' 		=> '082233806008',
                'first_name' 			=> 'Endras'
            ],
            [		'id' 			=> '57',
                'hp' 		=> '08156872726',
                'first_name' 			=> 'Edi W Akhsan'
            ]

        ]);

    }
}
