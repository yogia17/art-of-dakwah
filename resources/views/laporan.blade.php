@extends('frontLayout.laporan')
@section('title')
    Laporan
@stop

@section('style')

@stop
@section('content')
    <div class="content">
        <div class="row">
            <div class="col-md-1 col-sm-1  ">
            </div>
            <div class="col-md-10 col-sm-10  ">
                <div class="x_panel">
                    <div class="x_content">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Nama</th>
                                <th>HP</th>
                                <th>Waktu</th>
                                <th>Mark</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $i=1; ?>
                            @foreach ($user as $peserta)
                                <tr>
                                    <th>{{$i++}}</th>
                                    <th>{{$peserta->first_name}}</th>
                                    <th>{{$peserta->hp}}</th>
                                    <th>{{$peserta->h1s1}}</th>
                                    <th>
                                        @if(!empty($peserta->h1s1))
                                            <i class="fa fa-check" style="color:green;"></i>
                                        @endif
                                    </th>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="col-md-1 col-sm-1  ">
            </div>
        </div>

    </div>
@endsection

@section('scripts')


@endsection
