@extends('frontLayout.app')
@section('title')
    Home Page
@stop

@section('style')

@stop
@section('content')
    <div class="content">
        @if (Sentinel::check() )

        @else
                @if (Session::has('message'))
                    <div class="alert alert-{{(Session::get('status')=='error')?'danger':Session::get('status')}} " alert-dismissable fade in id="sessions-hide">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>{{Session::get('status')}}!</strong> {!! Session::get('message') !!}
                    </div>
                @endif
                {{ Form::open(array('url' => url('/userstore'), 'class' => 'form-horizontal form-signin')) }}
                <h3 class="form-signin-heading">Tambah peserta</h3>
                <hr class="colorgraph"><br>
                {!! csrf_field() !!}
                <div class="form-group {{ $errors->has('hp') ? 'has-error' : ''}}">
                    <div class="col-sm-12">
                        {!! Form::text('first_name', null, ['class' => 'form-control','placeholder '=>'Nama']) !!}
                        {!! Form::number('hp', null, ['class' => 'form-control','placeholder '=>'No HP (08232XXXX)']) !!}
                        {!! $errors->first('hp', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>

{{--                    <h4>Dimohon mengizinkan kamera Anda untuk scan QR Code</h4>--}}
                <button class="btn btn-lg btn-primary btn-block"  name="Submit" value="Login" type="Submit">Tambah</button>
                </form>
        @endif
    </div>
@endsection

@section('scripts')


@endsection
