@extends('frontLayout.customLayout')
@section('title')
Login
@stop
@section('content')
<div class = "container">
  <div class="wrapper">
    @if (Session::has('message'))
     <div class="alert alert-{{(Session::get('status')=='error')?'danger':Session::get('status')}} " alert-dismissable fade in id="sessions-hide">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
       <strong>{{Session::get('status')}}!</strong> {!! Session::get('message') !!}
      </div>
    @endif 
    {{ Form::open(array('url' => url('/step2'), 'class' => 'form-horizontal form-signin')) }}
        <h3 class="form-signin-heading">Presensi Sesi 1<br/>Art Of Dakwah #3</h3>
        <hr class="colorgraph"><br>
        {!! csrf_field() !!}
        <div class="form-group {{ $errors->has('hp') ? 'has-error' : ''}}">
            <div class="col-sm-12">
                {!! Form::number('hp', null, ['class' => 'form-control','placeholder '=>'Masukan No HP Anda']) !!}
                {!! $errors->first('hp', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
       
        <button class="btn btn-lg btn-primary btn-block"  name="Submit" value="Login" type="Submit">Scan QR Code</button>
    </form>
    
  </div>
</div>
@endsection

@section('scripts')


@endsection