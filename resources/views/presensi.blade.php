@extends('frontLayout.customLayout')
@section('title')
    Presensi {{$user->first_name}}
@stop

@section('style')

@stop
@section('content')
    <div class="content">
        <h3>Hai {{$user->first_name}},</h3>
        <div class="row">
            <div class="col-md-3 col-sm-3  ">
            </div>
            <div class="col-md-6 col-sm-6  ">
                <div class="x_panel">
                    <div class="x_content">
                        <table class="table table-striped">
                            <thead>
                            <tr>
{{--                                <th>Sesi</th>--}}
                                <th>Waktu Presensi</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
{{--                                <th>H1S1</th>--}}
                                <th>{{$user->h1s1}}</th>
                                <th>
                                    @if(!empty($user->h1s1))
                                        <i class="fa fa-check" style="color:green;"></i>
                                    @endif
                                </th>
                            </tr>
{{--                            <tr>--}}
{{--                                <th>H1S2</th>--}}
{{--                                <th>{{$user->h1s2}}</th>--}}
{{--                                <th>--}}
{{--                                    @if(!empty($user->h1s2))--}}
{{--                                        <i class="fa fa-check" style="color:green;"></i>--}}
{{--                                    @endif--}}
{{--                                </th>--}}
{{--                            </tr>--}}
{{--                            <tr>--}}
{{--                                <th>H2S1</th>--}}
{{--                                <th>{{$user->h2s1}}</th>--}}
{{--                                <th>--}}
{{--                                    @if(!empty($user->h2s1))--}}
{{--                                        <i class="fa fa-check" style="color:green;"></i>--}}
{{--                                    @endif--}}
{{--                                </th>--}}
{{--                            </tr>--}}

{{--                            <tr>--}}
{{--                                <th>H2S2</th>--}}
{{--                                <th>{{$user->h2s2}}</th>--}}
{{--                                <th>--}}
{{--                                    @if(!empty($user->h2s2))--}}
{{--                                        <i class="fa fa-check" style="color:green;"></i>--}}
{{--                                    @endif--}}
{{--                                </th>--}}
{{--                            </tr>--}}
{{--                            --}}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="col-md-3 col-sm-3  ">
            </div>
        </div>


    </div>
@endsection

@section('scripts')


@endsection
